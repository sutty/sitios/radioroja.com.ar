---
title: Peer Production License
description: Licencia del sitio y todo lo publicado
author:
- Sutty
permalink: licencia/
draft: true
order: 0
layout: post
uuid: 63418131-0bb1-4387-a32a-7eba65bf95ca
liquid: false
usuaries:
- 38
---



<p>This is a human-readable summary of the <a href="https://wiki.p2pfoundation.net/Peer_Production_License">full
license</a>.</p>
<h2>You are free to</h2>
<p><strong>Share</strong> -- copy and redistribute the material in any medium or
format</p>
<p><strong>Adapt</strong> -- remix, transform, and build upon the material</p>
<h2>Under the following terms:</h2>
<p><strong>Attribution</strong> -- You must give appropriate credit, provide a link
to the license, and indicate if changes were made. You may do so in
any reasonable manner, but not in any way that suggests the licensor
endorses you or your use.</p>
<p><strong>ShareAlike</strong> -- If you remix, transform, or build upon the
material, you must distribute your contributions under the same
license as the original.</p>
<p><strong>Non-Capitalist</strong> -- Commercial exploitation of this work is only
allowed to cooperatives, non-profit organizations and collectives,
worker-owned organizations, and any organization without
exploitation relations.  Any surplus value obtained by the exercise
of the rights given by this work's license must be distributed by
and amongst workers.'</p>
<h2>Notices:</h2>
<p>You do not have to comply with the license for elements of the
material in the public domain or where your use is permitted by an
applicable exception or limitation.</p>
<p>No warranties are given. The license may not give you all of the
permissions necessary for your intended use. For example, other
rights such as publicity, privacy, or moral rights may limit how you
use the material.</p>