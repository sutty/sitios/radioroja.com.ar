---
title: Licencia de Producción de Pares
description: Licencia del sitio y todo lo publicado
author:
- Sutty
permalink: licencia/
draft: true
order: 3
layout: post
uuid: a6b66cb4-d869-437a-9fc7-97d7c44105a1
liquid: false
usuaries:
- 38
---



<p>Esto es un resumen legible por humanas del <a href="http://endefensadelsl.org/ppl_es.html">texto legal (la licencia
completa)</a></p>
<h2>Ud. es libre de</h2>
<p><strong>Compartir</strong> - copiar, distribuir, ejecutar y comunicar públicamente la obra</p>
<p><strong>Adaptar</strong> - Hacer obras derivadas</p>
<h2>Bajo las condiciones siguientes:</h2>
<p><strong>Atribución</strong> - Debe reconocer los créditos de la obra de la manera
especificada por el autor o el licenciante (pero no de una manera
que sugiera que tiene su apoyo o que apoyan el uso que hace de su
obra).</p>
<p><strong>Compartir bajo la Misma Licencia</strong> - Si altera o transforma esta
obra, o genera una obra derivada, sólo puede distribuir la obra
generada bajo una licencia idéntica a ésta.</p>
<p><strong>No Capitalista</strong> - La explotación comercial de esta obra sólo está
permitida a cooperativas, organizaciones y colectivos sin fines de
lucro, a organizaciones de trabajadores autogestionados, y donde no
existan relaciones de explotación. Todo excedente o plusvalía
obtenidos por el ejercicio de los derechos concedidos por esta
Licencia sobre la Obra deben ser distribuidos por y entre los
trabajadores.</p>
<h2>Entendiendo que</h2>
<p><strong>Renuncia</strong> - Alguna de estas condiciones puede no aplicarse si se
obtiene el permiso del titular de los derechos de autor.</p>
<p><strong>Dominio Público</strong> - Cuando la obra o alguno de sus elementos se
halle en el dominio público según la ley vigente aplicable, esta
situación no quedará afectada por la licencia.</p>
<p><strong>Otros derechos</strong> - Los derechos siguientes no quedan afectados por
la licencia de ninguna manera:</p>
<p>Los derechos derivados de usos legítimos u otras limitaciones
reconocidas por ley no se ven afectados por lo anterior;</p>
<p>Los derechos morales del autor;</p>
<p>Derechos que pueden ostentar otras personas sobre la propia obra o
su uso, como por ejemplo derechos de imagen o de privacidad.</p>
<p>Aviso - Al reutilizar o distribuir la obra, tiene que dejar muy en
claro los términos de la licencia de esta obra. La mejor forma de
hacerlo es enlazar a esta página.</p>