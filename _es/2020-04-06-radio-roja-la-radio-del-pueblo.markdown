---
title: RADIO ROJA, LA RADIO DEL PUEBLO
description: Quienes Somos
author:
- Juan Martin
image:
  description: logo_radioroja
  path: public/z4xzg49u60kf62rozzog4sbwzx0j/logo completo 2019-color.pdf.png
order: 4
layout: post
uuid: 27ee763e-1d81-4bd3-9ef2-5e71e4980ba6
liquid: false
usuaries:
- 38
---



<div>Radio Roja es la unica Radio de Casa Grande, Valle de Punilla, provincia de Cordoba, Argentina, transmitiendo por FM 90.1 mhz y saliendo por streaming a todo el mundo. Somos parte de la RNMA, Red Nacional de Medios Alternativos de Argentina.</div>